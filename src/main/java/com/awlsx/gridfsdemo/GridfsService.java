package com.awlsx.gridfsdemo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.model.GridFSFile;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@RestController
public class Controller {

	public GridFsResource convertGridFSFile2Resource(GridFSFile gridFsFile) {
	    GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(gridFsFile.getObjectId());
	    return new GridFsResource(gridFsFile, gridFSDownloadStream);
	}

	@Resource
	private MongoDbFactory mongoDbFactory;

	@Bean
	public GridFSBucket getGridFSBuckets() {
	    MongoDatabase db = mongoDbFactory.getDb();
	    return GridFSBuckets.create(db);
	}

	@Resource
	private GridFSBucket gridFSBucket;

    @Autowired
    private GridfsService gridfsService;

    @RequestMapping(value = "/file/upload", method = RequestMethod.POST)
    public Object uploadData(@RequestParam(value = "file")MultipartFile file) {
    	
    	ObjectId id = gridfsService.save(file);
    	Map<String,Object> dt = new HashMap<String,Object>();
    	if(id == null) {
        	dt.put("id", id);
    	} else {
        	dt.put("id", id.toString());
    	}
    	return dt;
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping(value = "/file/delete", method = RequestMethod.POST)
    public Object deleteFile(@RequestParam(value = "id") String id) {

//        删除文件
        gridfsService.remove(id);
        return "delete ok";
    }


    /**
     * 下载文件
     * @param id
     * @param response
     */
    @RequestMapping(value = "/file/{id}", method = RequestMethod.GET)
    public void getFile(@PathVariable String id, HttpServletResponse response) {
    	GridFSFile file = gridfsService.getById(new ObjectId(id));

        if (file == null) {
            responseFail("404 not found",response);
            return;
        }

        OutputStream os = null;

        try {
            os = response.getOutputStream();
            response.addHeader("Content-Disposition", "attachment;filename=" + file.getFilename());
            response.addHeader("Content-Length", "" + file.getLength());
            response.setContentType("application/octet-stream");
            GridFsResource gridFsResource = convertGridFSFile2Resource(file);
            IOUtils.copy(gridFsResource.getInputStream(), os);
            os.flush();
            os.close();

        } catch (Exception e) {
            try{
                if (os != null) {
                    os.close();
                }
            }catch (Exception e2){}
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/file/view/{id}", method = RequestMethod.GET)
    public void viewFile(@PathVariable String id, HttpServletResponse response) {
    	GridFSFile file = gridfsService.getById(new ObjectId(id));

        if (file == null) {
            responseFail("404 not found",response);
            return;
        }

        OutputStream os = null;

        try {
            os = response.getOutputStream();
            response.addHeader("Content-Disposition", "attachment;filename=" + file.getFilename());
            response.addHeader("Content-Length", "" + file.getLength());
            if(file.getMetadata() != null) {
                response.setContentType(file.getMetadata().get("_contentType").toString());
            }
            GridFsResource gridFsResource = convertGridFSFile2Resource(file);
            IOUtils.copy(gridFsResource.getInputStream(), os);
            os.flush();
            os.close();

        } catch (Exception e) {
            try{
                if (os != null) {
                    os.close();
                }
            }catch (Exception e2){}
            e.printStackTrace();
        }

    }

    private void responseFail(String msg,HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        PrintWriter out = null;
        ObjectMapper mapper = new ObjectMapper();
        try{
            String res = mapper.writeValueAsString(msg);
            out = response.getWriter();
            out.append(res);
        } catch (Exception e){
            try {
                if (out != null) {
                    out.close();
                }
            }catch (Exception e2) {}
        }
    }


}
